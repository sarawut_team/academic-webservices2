const { Router } = require('express')
const academic = require('../ethereum/academic')
const router = Router()

async function getStudentById(id) {
    const studentInfo = await academic.methods.getStudentInfo(id).call()
    const studentGrade = await academic.methods.getStudentGrade(id).call()
    return {
        ID: studentInfo[0],
        Wallet: studentInfo[1],
        Fullname: studentInfo[2],
        Department: studentInfo[3],
        Faculty: studentInfo[4],
        Semester_01: JSON.parse(studentGrade[0]),
        Semester_02: JSON.parse(studentGrade[1]),
        Semester_03: JSON.parse(studentGrade[2]),
        Semester_04: JSON.parse(studentGrade[3]),
    }
}

async function getStudentByWallet(wallet) {
    const studentInfo = await academic.methods.getSTDinfobyWallet(wallet).call()
    const studentGrade = await academic.methods.getSTDGradebyWallet(wallet).call()
    return {
        ID: studentInfo[0],
        Wallet: studentInfo[1],
        Fullname: studentInfo[2],
        Department: studentInfo[3],
        Faculty: studentInfo[4],
        Semester_01: JSON.parse(studentGrade[0]),
        Semester_02: JSON.parse(studentGrade[1]),
        Semester_03: JSON.parse(studentGrade[2]),
        Semester_04: JSON.parse(studentGrade[3]),
    }
}

router.get('/ids/:id', (req, res) => {
    getStudentById(req.params.id).then((data) => {
        res.json(data)
    })
})

router.get('/wallets/:wallet', (req, res) => {
    getStudentByWallet(req.params.wallet).then((data) => {
        res.json(data)
    })
})

module.exports = router