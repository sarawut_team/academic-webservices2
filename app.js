const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const routes = require('./routes')
const port = process.env.PORT || 5001
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/students', routes)

app.listen(port, () => console.log('listen in port ', port))

module.exports = app